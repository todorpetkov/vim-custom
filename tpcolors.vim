" local syntax file - set colors on a per-machine basis:
" Maintainer:   Todor Petkov <petkovptodor@gmail.com>
" Copied from unknown source from Internet. If you know the original author, let me know.
" Last Change:  2020 Feb 14

hi clear
set background=dark
if exists("syntax_on")
        syntax reset
endif

let colors_name = "tpcolors"

hi Normal         term=none     cterm=none      ctermfg=gray            ctermbg=black
hi SpecialKey     term=none     cterm=none      ctermfg=darkgray        ctermbg=bg
hi NonText        term=none     cterm=none      ctermfg=darkgray        ctermbg=bg
hi Directory      term=none     cterm=none      ctermfg=lightblue       ctermbg=bg
hi ErrorMsg       term=none     cterm=bold      ctermfg=yellow          ctermbg=darkred
hi IncSearch      term=none     cterm=none      ctermfg=white           ctermbg=darkcyan
hi Search         term=none     cterm=underline ctermfg=fg              ctermbg=bg
hi MoreMsg        term=none     cterm=none      ctermfg=black           ctermbg=gray
hi ModeMsg        term=none     cterm=bold      ctermfg=white           ctermbg=bg
hi LineNr         term=none     cterm=none      ctermfg=darkgreen       ctermbg=bg
hi Question       term=none     cterm=none      ctermfg=black           ctermbg=gray
hi StatusLine     term=none     cterm=bold      ctermfg=black           ctermbg=gray
hi StatusLineNC   term=none     cterm=bold      ctermfg=gray            ctermbg=darkblue
hi VertSplit      term=none     cterm=bold      ctermfg=gray            ctermbg=darkblue
hi Title          term=none     cterm=bold      ctermfg=white           ctermbg=bg
hi Visual         term=none     cterm=none      ctermfg=fg              ctermbg=darkblue
hi VisualNOS      term=none     cterm=none      ctermfg=fg              ctermbg=darkblue
hi WarningMsg     term=none     cterm=bold      ctermfg=red             ctermbg=bg
hi WildMenu       term=none     cterm=none      ctermfg=gray            ctermbg=darkblue
hi Folded         term=none     cterm=none      ctermfg=yellow          ctermbg=blue
hi FoldColumn     term=none     cterm=none      ctermfg=yellow          ctermbg=blue
hi DiffAdd        term=none     cterm=none      ctermfg=fg              ctermbg=darkblue
hi DiffChange     term=none     cterm=none      ctermfg=fg              ctermbg=darkmagenta
hi DiffDelete     term=none     cterm=none      ctermfg=fg              ctermbg=darkred
hi DiffText       term=none     cterm=inverse   ctermfg=fg              ctermbg=bg
hi Cursor         term=inverse  cterm=inverse   ctermfg=bg              ctermbg=fg

" syntax highlighting follows:

hi Comment        term=none     cterm=none      ctermfg=blue            ctermbg=bg
hi Constant       term=none     cterm=none      ctermfg=darkcyan        ctermbg=bg
hi Special        term=none     cterm=none      ctermfg=gray            ctermbg=bg
hi Identifier     term=none     cterm=none      ctermfg=white           ctermbg=bg
hi Statement      term=none     cterm=none      ctermfg=yellow          ctermbg=bg
hi PreProc        term=none     cterm=none      ctermfg=cyan            ctermbg=bg
hi Type           term=none     cterm=none      ctermfg=darkgreen       ctermbg=bg
hi Underlined     term=none     cterm=underline ctermfg=fg              ctermbg=bg
hi Ignore         term=none     cterm=none      ctermfg=darkblue        ctermbg=bg
hi Error          term=none     cterm=none      ctermfg=gray            ctermbg=darkred
hi Todo           term=none     cterm=none      ctermfg=yellow          ctermbg=darkmagenta
