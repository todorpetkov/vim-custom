"" You can load the file like this
"" Source a global configuration file if available
"if filereadable("/etc/vim/vimrc-local.vim")
"  source /etc/vim/vimrc-local.vim
"endif

set autowrite                   " Automatically save before commands like :next and :make
set backspace=indent,eol,start  " More powerful backspacing
set cmdheight=1
set comments=b:#,b:\",n:>
set hidden                      " Hide buffers when they are abandoned
set hlsearch                    " Hightlight search
set ignorecase                  " Do case insensitive matching
set incsearch                   " Incremental search
set joinspaces
set laststatus=2
set linebreak                   " Don't wrap words by default
set magic
set noautoindent
set nocindent
set nocompatible
set paste
set ruler
set scrolloff=5
set shortmess=o
set showcmd                     " Show (partial) command in status line
set showmatch                   " Show matching brackets
set showmode
set smartcase                   " Do smart case matching
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}][TYPE=%Y]\ \ \ [POS=%l,%v][%p%%]\ %{strftime(\"%d/%m/%y\ -\ %H:%M\")}
set t_Co=256                    " set number of colors to 256
set textwidth=0                 " don't wrap lines by default
set ts=8 sts=4 sw=4 noet        " hardtab 8, softtab 4, shiftwidth 4, no expandtab
set viminfo='20,\"50,h
set wildmenu
set wildmode=longest,list,full
set wrap
set mouse=r

autocmd BufAdd * exe 'tablast | tabe "' . expand( "<afile") .'"'
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
" Disable mkview/loadview, since it requires ~/.vim directory present to create view subdirectory, which is not always the case and leads to warnings
"autocmd BufWinEnter *.* silent loadview
"autocmd BufWinLeave *.* mkview
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" The above BufWinEnter setting flashes annoyingly while typing, be calmer in insert mode
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd VimEnter * tab all
autocmd BufAdd * exe 'tablast | tabe "' . expand( "<afile") .'"'

highlight CursorLine term=bold cterm=bold guibg=Grey40
highlight ExtraWhitespace ctermbg=darkred guibg=#382424
highlight TrailingWhitespace ctermbg=red guibg=red
highlight StatusLine ctermbg=black ctermfg=black

" Show trailing whitespaces
match TrailingWhitespace /\s\+$/

filetype plugin on

" Run :FixWhitespace to remove end of line white space
function! s:FixWhitespace(line1,line2)
    let l:save_cursor = getpos(".")
    silent! execute ':' . a:line1 . ',' . a:line2 . 's/\s\+$//'
    call setpos('.', l:save_cursor)
endfunction
command! -range=% FixWhitespace call <SID>FixWhitespace(<line1>,<line2>)

" If you want to have template for .sh files, create it as ~/.vim/templates/skeleton.sh and uncomment below
"if has("autocmd")
"  augroup templates
"    autocmd BufNewFile *.sh 0r ~/.vim/templates/skeleton.sh
"  augroup END
"endif

" Editing large viles
let g:LargeFile=100

" Loading colorscheme
colorscheme onehalfdark

" Enable syntax
syntax on

" Set space key as shortcut
let mapleader = "\<Space>"
" Space-w saves the file
nnoremap <Leader>w :w<CR>
" Space-x saves and exits the file
nnoremap <Leader>x :x<CR>
" Space-q exits the file
nnoremap <Leader>q :q<CR>
" Space-n goes to next window
nnoremap <Leader>n :next<CR>
" Space-p goes to previous window
nnoremap <Leader>p :previous<CR>
" Space-space marks the current line
nmap <Leader><Leader> V
