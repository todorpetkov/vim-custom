Custom VIM settings.

vimrc.local can be added to vimrc or included from it. distinguished.vim and tpcolors.vim need to be added in local or global colorscheme directory.


Steps to download files on Debian (assuming you have sudo access and vim is already installed):


sudo mkdir /etc/vim/colors;

sudo curl -o /etc/vim/vimrc.local https://gitlab.com/todorpetkov/vim-custom/-/raw/main/vimrc.local;

```bash
for _colorsheme in distinguished gruvbox molokai onehalfdark onehalflight tpcolors;
  do sudo curl -o /etc/vim/colors/${_colorsheme}.vim https://gitlab.com/todorpetkov/vim-custom/-/raw/main/${_colorsheme}.vim;
done
```

# Allow mouse to copy/paste in vim like usual
sudo sed -i -e 's#\(mouse=\)a$#\1r#' /usr/share/vim/*/defaults.vim
